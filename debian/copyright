Format: https://www.debian.org/doc/packaging-manuals/copyright-format/1.0/
Upstream-Name: python-llfuse
Upstream-Contact: Nikolaus Rath <Nikolaus@rath.org>
Source: https://github.com/python-llfuse/python-llfuse
Files-Excluded: doc/html

Files: *
Copyright: 2010-2015  Nikolaus Rath <Nikolaus.org>
License: LGPL-2+

Files: examples/*
Copyright: 2013, 2015  Nikolaus Rath <Nikolaus.org>
           2015  Gerion Entrup
License: modified-modern-MIT-with-sublicense
Comment: In comparison to the Expat License, this license's first stanza
 is missing the trailing ", subject to the following conditions:".  The
 second stanza is missing completely, and it usually contains "The above
 copyright notice and this permission notice shall be included in all
 copies or substantial portions of the Software."  The name "Modern MIT
 with sublicense" comes from Fedora license documentation:
 https://fedoraproject.org/wiki/Licensing:MIT#Modern_Style_with_sublicense

Files: src/darwin_compat*
Copyright: 2006-2008  Amit Singh/Google Inc.
           2012  Anatol Pomozov
           2011-2013  Benjamin Fleischer
License: LGPL-2+

Files: test/pytest_checklogs.py
Copyright: 2008  Nikolaus Rath <Nikolaus@rath.org>
License: GPL-3+

Files: test/test_rounding.py
Copyright: 2020 Philip Warner <philipwarner.info>
License: LGPL-2+

Files: debian/*
Copyright: 2011-2019  Nikolaus Rath <Nikolaus@rath.org>
           2020-2021  Nicholas D Steeves <sten@debian.org>
License: LGPL-2+

License: LGPL-2+
 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU Lesser General Public License as
 published by the Free Software Foundation, either version 2 of the
 License, or (at your option) any later version.
 .
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU Lesser General Public License for more details.
 .
 On Debian systems the full text of the GNU Lesser General Public
 License can be found in the `/usr/share/common-licenses/LGPL-2'
 file.

License: modified-modern-MIT-with-sublicense
 Permission is hereby granted, free of charge, to any person obtaining
 a copy of this software and associated documentation files (the
 "Software"), to deal in the Software without restriction, including
 without limitation the rights to use, copy, modify, merge, publish,
 distribute, sublicense, and/or sell copies of the Software, and to
 permit persons to whom the Software is furnished to do so.
 .
 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS
 BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN
 ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
 CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 SOFTWARE.

License: GPL-3+
 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
 .
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 .
 On Debian systems, the complete text of the GNU General
 Public License version 3 can be found in "/usr/share/common-licenses/GPL-3".
